/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaactivity;

import java.util.*;


public class JavaActivity {

    public static void main(String[] args) {
        
         //=========================== Number 1 ====================================
        System.out.println("Number 1: ");
        addAndSort();
        
         //=========================== Number 2 ====================================
         
        System.out.println("Number 2: ");
        randomArrayList();  
        
         //=========================== Number 3 ====================================
        
        List<Integer> numList = new ArrayList<>();
        
        numList.add(3);
        numList.add(8);
        numList.add(23);
        numList.add(91);
        numList.add(6);
        numList.add(1);
        
        System.out.println("Number 3: ");
        System.out.println("Value: " + moveMinimumValue(numList) + "\n");
        
        
        //=========================== Number 4 ====================================
        
        System.out.println("Number 4: ");
        
        System.out.println("The top reasons for using '.isempty' rather than 'size' because , it is more expressive\n "
                + "(the code is easier to read and to maintain)"
                +"and it is faster, in some cases by orders of magnitude. "
                + "\nTwo examples from the jdk where this is "
                + "extremely visible would be the concurrentlinkedqueue and "
                + "navigablemap / navigableset."
                + " \nUnlike 'size' method only implement by "
                + "iterating trough the collection and because "
                + "of this, calling size gets \nincreasingly slower as the "
                + "number of elements increase.\n");
        
        //=========================== Number 4 ====================================
        
        System.out.println("Number 5: ");
        
        System.out.println("The biggest differences are that a foreach loop processes an instance of each"
                + " element in a collection in turn, \nwhile a for loop can work with any data and is not "
                + "restricted to collection elements alone. This means that a for \nloop can modify a "
                + "collection - which is illegal and will cause an error in a foreach loop.\n");
        System.out.println("The forEach() method provides several advantages over the traditional"
                + " for loop e.g. \n"
                + "you can execute it in parallel by just using a parallel Stream "
                + "instead of regular stream. \nSince you are operating on stream, it also allows "
                + "you to filter and map elements.\n");
        
       
        //=========================== Number 6 ====================================
     
        Set<Integer> arrset1 = new HashSet<>();
  
        // Populating arrset1
        arrset1.add(96);
        arrset1.add(89);
        arrset1.add(3);
        arrset1.add(78);
        arrset1.add(57);
            
        Set<Integer> arrset2 = new HashSet<>();
  
        arrset2.add(6);
        arrset2.add(96);
        arrset2.add(5);
        arrset2.add(78);
        arrset2.add(56);
        arrset2.add(57);
        
        System.out.println("Number 6: ");
        System.out.println("First Set: " + arrset1);
        System.out.println("Second Set: " + arrset2); 
        System.out.println("Common Values: " + compareSet(arrset1, arrset2) + "\n");
        
        
        //=========================== Number 7 ====================================
        //number 7
        
        Set<String> winterFruits = new HashSet<>();
     
         winterFruits.add("Clementine");
         winterFruits.add("Plums");
         winterFruits.add("Dates");
         winterFruits.add("Grapefruit");
         winterFruits.add("Kiwi");
          
        Set<String> summerFruits = new HashSet<>();
          summerFruits.add("Mango");
          summerFruits.add("Plums");
          summerFruits.add("Watermelon");
          summerFruits.add("Papaya");
          summerFruits.add("Grapefruit");
        
          
        System.out.println("Number 7: ");
        System.out.println("First set: " +  winterFruits);
        System.out.println("First set: " +  summerFruits);
        System.out.println("Unique Values: " + getUniqueValue(winterFruits, summerFruits) + "\n");
          
        //=========================== Number 8 ====================================
        //number 7
        
    
       Map<String, String> map  = new HashMap<>(); 
       
        map.put("concordia1", "Value given");
        map.put("acconcordia", "Test");
        map.put("condensada", "Sweet");

        System.out.println("Number 8: ");
        System.out.println("Map: "+ map);
        System.out.println("Number of Key: " + countKeys(map));
        
        
     
    }
    
    
         
     
     
     //===========================|| Methods || ====================================
    
    
    //number 1
    static void addAndSort(){
            
        List<String> nameList = new ArrayList<>();
        
        nameList.add("Ric Jee");
        nameList.add("Martin");
        nameList.add("Victioriano");
        nameList.add("James Aldrin");
        nameList.add("Jurick");
        
        System.out.println("Before Sorting: "+ nameList);   
        // Sorting ArrayList in ascending Order   
        Collections.sort(nameList);   
        // printing the sorted ArrayList   
        System.out.println("After Sorting: "+ nameList+"\n"); 
             
        }
     
     //=========================== Method 2 ====================================
    //number 2
     static void randomArrayList() {
         
         List<String> nameList = new ArrayList<>();
        
        nameList.add("Ric Jee");
        nameList.add("Martin");
        nameList.add("Victioriano");
        nameList.add("James Aldrin");
        nameList.add("Jurick");
        
        System.out.println("Before Shuffle: "+ nameList);   
        // Sorting ArrayList in ascending Order   
        Collections.shuffle(nameList);   
        // printing the sorted ArrayList   
        System.out.println("After Shuffle: "+ nameList+"\n"); 
             
     }
      //=========================== Method 3 ====================================
     //Number 3
     static List<Integer> moveMinimumValue(List<Integer> list){
         
            int min = 0;
         
            for (int i = 1; i < list.size(); i++) {
                if (list.get(i) < list.get(min)) {
                    min = i;
                }
            }
            
            int oldValue = list.remove(min);
	    list.add(0, oldValue);
            
            return list;
	    // could also have been written in one line:
            // list.add(0, list.remove(min));
         
     }
     
     //=========================== Method 6 ====================================
    //number 6
    static Set<Integer> compareSet(Set<Integer> arrset1, Set<Integer> arrset2) {
        
        // comparing first Set to another
        arrset1.retainAll(arrset2);
        return arrset1;
    }
    
    //number 7
    static Set<String> getUniqueValue(Set<String> winterFruits, Set<String> summerFruits){
        
        // comparing first Set to another
        winterFruits.addAll(summerFruits);
        Set <String> uniqueValues = new HashSet<>(winterFruits);
        return uniqueValues;
        
    }
    
    
    //number 8
    static int countKeys(Map<String, String> map) {
        
        int count = 0;
        
        for (String k : map.keySet()){
            
             if(k.startsWith("concordia")){
                  count++;
             }
             
         }
        
        return count;
    }
}
